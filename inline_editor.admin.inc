<?php

/**
 * @file
 * Provides the inline editor module admin pages.
 */

/**
 * Renders form with all entities.
 */
function inline_editor_admin_main() {

  $entities = entity_get_info();

  // Get fieldable entities.
  foreach ($entities as $name => $entity) {
    if (!$entity['fieldable']) {
      unset($entities[$name]);
    }
  }

  $form = drupal_get_form('inline_editor_admin_select_entities_form', $entities);
  return drupal_render($form);
}

/**
 * Returns form with all entities.
 */
function inline_editor_admin_select_entities_form($form, $form_state, $entities) {

  $entities_editable = variable_get('inline_editor_editable_entities', array());

  foreach ($entities as $key => $entity) {
    $entities_opt[$key] = $entity['label'];
  }

  $form['selected_entities'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Select entities'),
    '#options' => $entities_opt,
    '#default_value' => $entities_editable,
    '#description' => t('Select the entities you want to be editable.'),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  return $form;
}

/**
 * Implements hook_form_submit().
 */
function inline_editor_admin_select_entities_form_submit($form, $form_state) {
  $result = array();
  foreach ($form_state['values']['selected_entities'] as $field_name => $value) {
    if ($value) {
      $result[] = $field_name;
    }
  }
  variable_set('inline_editor_editable_entities', $result);
  drupal_set_message(t('The configuration have been saved.'));
}
