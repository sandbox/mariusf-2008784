/**
 * @file
 * Attaches behavior for the Inline editor module.
 */

( function($) {
  var inlineEditorAjax = null;
  Drupal.behaviors.inline_editor = {
    attach: function (context, settings) {
      if ('ajax' in Drupal) {
        Drupal.ajax.prototype.commands.inlineEditorAfterFormSubmit = function() {
          var $fieldEditContainer = $('.inline-editor-edit-in-use'),
              $field = $fieldEditContainer
                        .removeClass('inline-editor-edit-in-use')
                        .find('.field, .entity-field-collection-item')
                          .filter(':first');

          // If replaceWidth return an extra div, remove it.
          if ($field.parent('.inline-editor-editable').length == 0) {
            $field.parent('div').remove();
            $fieldEditContainer.append($field);
          }

          inlineEditorUnlock();
        }

        Drupal.ajax.prototype.commands.inlineEditorScrollFix = function() {
          var marginTop = $('#field-collection-item-form').offset().top - 135;
          $("html").animate({ scrollTop: marginTop }, 300);
        }

        Drupal.ajax.prototype.commands.inlineEditorCancelEdit = function() {
          $('.inline-editor-cancel-edit').click();
        }

        Drupal.ajax.prototype.commands.inlineEditorFocusInput = function() {
          var $form = $('#inline-editor-get-form'),
              $input = $form.find('input[type="text"]').filter(':first');

          $input.focus().val($input.val());
        }

        Drupal.ajax.prototype.commands.inlineEditorAfterFormReceived = function() {
          document.body.style.cursor = 'default';
        }

        Drupal.ajax.prototype.commands.inlineEditorAfterCollectionSubmit = function(ajax, response) {
          var addNewBtn = $.data(document.body, 'inline-editor-add-new-btn'),
              fieldName = '.field-name-' + response.fieldName.replace(/_/g, '-');
          $(addNewBtn).removeClass('inline-editor-edit-in-use');
          $(fieldName).removeClass('inline-editor-edit-in-use').before(addNewBtn);
          $.removeData(document.body, 'inline-editor-add-new-btn'); 
          inlineEditorUnlock();
        }

        Drupal.ajax.prototype.getForm = function(entityType, entityId, fieldName, viewMode, lang) {
          var ajax = this;
          if (entityType == 'field_collection_item') {
            var hostEntity = $('.inline-editor-edit-in-use')
                              .closest('.field-collection-container')
                                .children('.inline-editor-new-item')
                                  .attr('data-inline-editor-extra-info')
                                  .split('/');
            ajax.options.url = Drupal.settings.basePath + 
              'inline-editor-collection/get-ajax-form/' + entityId + '/' + 
              entityType + '/' + fieldName + '/' + hostEntity[0] + '/' + 
              hostEntity[1] + '/' + viewMode;
          }
          else {
            ajax.options.url = Drupal.settings.basePath + 
              'inline-editor/get-ajax-form/' + entityType + '/' + entityId + 
              '/' + fieldName + '/' + viewMode + '/' + lang;
          }

          var f = ajax.options.success;
          ajax.options.success = (function(data) {
            if(data[0].settings.select2widgetNew !== undefined) {
              Drupal.settings.select2widgetNew = data[0].settings.select2widgetNew;
            }
            f(data);
          });

          // Do not perform another ajax command if one is already in progress.
          if (ajax.ajaxing) {
            return false;
          }

          try {
            $.ajax(ajax.options);
          }
          catch (err) {
            alert('An error occurred while attempting to process ' + ajax.options.url);
            return false;
          }
        };

        if (!inlineEditorAjax) {
          // Define a custom ajax action not associated with an element.
          var inlineGetFormSettings = {};
          inlineGetFormSettings.url = Drupal.settings.basePath + 
            'inline-editor/get-ajax-form/';
          inlineGetFormSettings.event = 'onload';
          inlineGetFormSettings.keypress = false;
          inlineGetFormSettings.prevent = false;
          inlineGetFormSettings.effect = 'fade';
          inlineEditorAjax = new Drupal.ajax('inline_editor_ajax_action', $(document.body), inlineGetFormSettings);
        }
      }
    }
  }

  $(document).ready(function() {

    $("body").delegate('.inline-editor-editable', "mouseenter", function() {

      var $editButton = $(this).find('.inline-editor-button');
      if ($editButton.hasClass('locked')) return;
      $(this).children('.field, .entity')
               .addClass('inline-editor-editable-locked');
      $editButton.css('visibility', 'visible');
    });

    $('body').delegate('.inline-editor-editable', 'mouseleave', function() {
      $(this).children('.field, .entity')
               .removeClass('inline-editor-editable-locked');

      var $editButton = $(this).find('.inline-editor-button');

      if ($editButton.hasClass('locked')) {
        return;
      }
      
      $(this).find('.inline-editor-button').css('visibility', 'hidden');
    });

    $('body').delegate('.inline-editor-editable a', 'click', function(e) {
      // Prevent some errors.
      e.preventDefault();
    });

    $('body').delegate('.inline-editor-editable', 'click', function(e) {
      if ($('.inline-editor-editable, .inline-editor-new-item')
        .hasClass('inline-editor-edit-in-use')) {
        return;
      }
      $(this).find('.inline-editor-button').click();
    });

    $('body').delegate('.inline-editor-button', 'click', function(e) {
      if ($('.inline-editor-editable, .inline-editor-new-item')
          .hasClass('inline-editor-edit-in-use')) {
        return;
      }

      inlineEditorLock();

      $(this).closest('.field').children('.field-label')
                                 .removeClass('locked-editable');
      var tD = $(this).closest('.inline-editor-editable')
                                .addClass('inline-editor-edit-in-use')
                                .attr('data-inline-editor-data')
                                .split('/');
      var $field = $(this).closest('.inline-editor-editable')
                            .find('.field, .entity-field-collection-item')
                            .filter(':first');
      var htmlTarget = $field[0].outerHTML;

      $.data(document.body, 'inline-editor-prev-data', htmlTarget);
      $field.addClass('inline-editor-current-edit');
      inlineEditorAjax.getForm(tD[0], tD[1], tD[2], tD[3], tD[4]);
    });

    $('body').delegate('.inline-editor-new-item', 'click', function(e) {
      inlineEditorLock();
      var tD = $(this).addClass('inline-editor-edit-in-use')
                              .attr('data-inline-editor-data')
                              .split('/');

      $.data(document.body, 'inline-editor-add-new-btn', this);

      $(this).closest('.field-collection-container')
               .addClass('inline-editor-insert-new-item');
      $(this).siblings('.field')
               .find('.field-items')
               .filter(':first')
               .before('<div id="inline-editor-new-item" />');

      inlineEditorAjax.getForm(tD[0], tD[1], tD[2], tD[3], tD[4]);
    });

    $('body').delegate('.form-autocomplete', 'keydown', function(e) {
      if (e.which == 13 && $('#autocomplete').length > 0) {
        e.preventDefault();
        e.stopPropagation();
      }
    });

    $('body').delegate('.inline-editor-cancel-edit','click' , function(e) {
      e.stopPropagation();

      if (typeof Drupal.behaviors.ckeditor != 'undefined') {
        Drupal.behaviors.ckeditor.detach(document, Drupal.settings, 'unload');
      }

      var $fieldEdit = $(this).closest('.inline-editor-current-edit'),
          data = $.data(document.body, 'inline-editor-prev-data');

      $.removeData(document.body, 'inline-editor-prev-data');

      $fieldEdit.closest('.inline-editor-edit-in-use')
                  .removeClass('inline-editor-edit-in-use');
      $fieldEdit.replaceWith(data);
      inlineEditorUnlock();
    });

    $('body').delegate('.inline-editor-cancel-new-item', 'click', function() {
      inlineEditorUnlock();
      if (typeof Drupal.behaviors.ckeditor != 'undefined') {
        Drupal.behaviors.ckeditor.detach(document, Drupal.settings, 'unload');
      }
      $('#inline-editor-new-item').remove();
      $.removeData(document.body, 'inline-editor-add-new-btn');
      $('.inline-editor-insert-new-item')
        .removeClass('inline-editor-insert-new-item')
        .find('.inline-editor-edit-in-use')
        .removeClass('inline-editor-edit-in-use');
    });

    $('body').delegate('.field-collection-item-delete', 'click', function() {
      document.body.style.cursor = 'wait';
      var itemId = $(this).css('cursor', 'wait')
                          .attr('disabled', 'disabled')
                          .attr('item-id');

      $.ajax({
        url: Drupal.settings.basePath + 'delete-field-collection-item',
        dataType: 'json',
        type: "POST",
        data: { data: itemId },
        success: function(data) {
          if (data.deleted) {
            $('.inline-editor-edit-in-use')
              .closest('.field-item')
              .fadeOut(300, function(){
                $(this).remove();
              });
            
            $.removeData(document.body, 'inline-editor-prev-data');
            
            if (typeof Drupal.behaviors.ckeditor != 'undefined') {
              Drupal.behaviors.ckeditor.detach(document, Drupal.settings, 'unload');
            }
            inlineEditorUnlock();
          }
        },
        error: function(data) {
          document.body.style.cursor = 'default';
        },
      });
    });

    $('body').delegate('.inline-editor-choose-file', 'click', function() {
      $(this).next('input:file').click();
    });
  });

  function inlineEditorLock() {
    $('.field, .entity').removeClass('inline-editor-editable-locked');
    $('.inline-editor-button').addClass('locked');
    $('.inline-editor-new-item, .inline-editor-button')
      .css('visibility','hidden');
    $('.field-type-field-collection .field-label, .inline-editor-editable, .inline-editor-hidden')
      .addClass('locked-editable');
    document.body.style.cursor = 'wait';
  }

  function inlineEditorUnlock() {
    $('.field-label, .inline-editor-editable, .inline-editor-hidden')
      .removeClass('locked-editable');
    $('.inline-editor-button').removeClass('locked');
    $('.inline-editor-new-item').css('visibility','visible');
    document.body.style.cursor = 'default';
  } 
})(jQuery);