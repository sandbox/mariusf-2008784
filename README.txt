Description
-----------
  Inline editor provides a way to edit any field on any entity. 
  For Long text fields Ckeditor is used (if available).
  This is a more light-weight, configurable module than other modules available for in-place editing.

Installation
------------
  1. Download and install like any other Drupal module.
  2. Go to /admin/config/content/inline-editor and select entity which will be editable.
  3. Go to /admin/structure/types/manage/%type/display and select for each display fields which will be editable.

Differences between Inline editor and Edit
------------------------------------------
  - You can select what entity types you want to be editable
  - You can choose per view mode what fields will be available for in-place editing.
  - No need for other libraries.
  - Edit field collection item separately, not entire field collection entity.

Dependencies:
-------------
Entity API