<?php

/**
 * @file
 * Provides inline editing for fields.
 *
 * This module adds a wrapper to each field, enabling them for inline editing.
 */

/**
 * Implements hook_menu().
 */
function inline_editor_menu() {
  $items['inline-editor/get-ajax-form'] = array(
    'page callback' => 'inline_editor_get_field_form_ajax',
    'page arguments' => array(2, 3, 4, 5, 6),
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK,
  );
  $items['inline-editor-collection/get-ajax-form'] = array(
    'page callback' => 'inline_editor_get_field_collection_form_ajax',
    'page arguments' => array(2, 3, 4, 5, 6, 7),
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK,
  );
  $items['delete-field-collection-item'] = array(
    'page callback' => 'inline_editor_delete_field_collection_item',
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK,
  );
  $items['admin/config/content/inline-editor'] = array(
    'title' => 'Inline Editor',
    'description' => 'Configure the inline editor.',
    'page callback' => 'inline_editor_admin_main',
    'file' => 'inline_editor.admin.inc',
    'access arguments' => array('administer content'),
    'type' => MENU_NORMAL_ITEM,
  );
  return $items;
}

/**
 * Implements hook_init().
 */
function inline_editor_init() {
  if (!isset($_SERVER['HTTP_X_REQUESTED_WITH']) ||
    strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest') {
    if (isset($_SESSION['js_css_already_loaded'])) {
      unset($_SESSION['js_css_already_loaded']);
    }
  }
}

/**
 * Added required library.
 */
function inline_editor_process(&$variables, $hook) {
  // Required for the loaded form to be ajaxy.
  drupal_add_library('system', 'drupal.ajax');
  drupal_add_library('system', 'jquery.form');
  drupal_add_js("misc/autocomplete.js");
}

/**
 * Implements hook_entity_view_alter().
 */
function inline_editor_entity_view_alter(&$build, $type) {
  if (!in_array($type, variable_get('inline_editor_editable_entities', array()))) {
    return;
  }

  if ($type == 'node') {
    $entity = $build['#node'];
  }
  else {
    $entity = $build['#entity'];
  }

  if (!entity_access('update', $type, $entity) || (isset($entity->is_preview) &&
      $entity->is_preview)) {
    return;
  }

  $bundle = $build['#bundle'];
  $view_mode = $build['#view_mode'];
  $lang = $build['#language'];
  $edit_btn = '<div class="inline-editor-button"><i class="icon-pencil"></i>'
   . t('Edit') . '</div>';

  list($id) = entity_extract_ids($type, $entity);

  if ($type == 'field_collection_item') {
    $host_type = $entity->hostEntityType();
    $host_bundle = $entity->hostEntityBundle();
    $field_name = $entity->field_name;
    $field_info = field_info_instance($host_type, $field_name, $host_bundle);

    if (!empty($field_info['settings']['editable'][$view_mode])) {
      $edit_div = '<div class="inline-editor-editable"'
       . "data-inline-editor-data='$type/$id/$field_name/$view_mode/$lang'>";

      $edit_div .= $edit_btn;

      $build['#prefix'] = isset($build['#prefix']) ?
        $build['#prefix'] . $edit_div : $edit_div;

      $build['#suffix'] = isset($build['#suffix']) ?
        '</div>' . $build['#suffix'] : '</div>';
    }
  }
  else {
    $fields = field_info_instances($type, $bundle);
    foreach ($fields as $field_name => $field) {
      if (!empty($field['settings']['editable'][$view_mode]) &&
          isset($build[$field_name])) {
        if ($field['widget']['module'] == 'field_collection') {
          $add_button = '<div class="inline-editor-new-item"'
           . "data-inline-editor-data='field_collection_item/0/$field_name/"
           . "$view_mode/$lang' data-inline-editor-extra-info='$type/$id'>"
           . "<i class='icon-plus'></i>" . t('Add') . "</div>";

          $build[$field_name]['#prefix'] = isset($build[$field_name]['#prefix']) ?
          $build[$field_name]['#prefix'] . $add_button : $add_button;
        }
        else {
          $edit_div = "<div class='inline-editor-editable'"
           . "data-inline-editor-data='$type/$id/$field_name/$view_mode/$lang'>";

          $edit_div .= $edit_btn;

          $build[$field_name]['#prefix'] = isset($build[$field_name]['#prefix']) ?
            $build[$field_name]['#prefix'] . $edit_div : $edit_div;

          $build[$field_name]['#suffix'] = isset($build[$field_name]['#suffix']) ?
            '</div>' . $build[$field_name]['#suffix'] : '</div>';
        }
      }
    }
  }

  // Load css and js.
  $path = drupal_get_path('module', 'inline_editor');
  $build['#attached']['js'][] = $path . '/inline_editor.js';
  $build['#attached']['css'][] = $path . '/inline_editor.css';
}

/**
 * Implements hook_field_attach_view_alter().
 *
 * Show titles of empty fields.
 */
function inline_editor_field_attach_view_alter(&$output, $context) {
  $t = get_t();

  $entity_type = $context['entity_type'];
  if (!in_array($entity_type, variable_get('inline_editor_editable_entities', array())) ||
      $entity_type == 'field_collection_item') {
    return;
  }
  $entity = $context['entity'];
  list(, , $bundle) = entity_extract_ids($entity_type, $entity);
  // Load all instances of the fields for the entity.
  $instances = field_info_instances($entity_type, $bundle);

  if (isset($entity->is_preview) && $entity->is_preview) {
    // In preview remove empty field. (field collections).
    foreach ($instances as $field_name => $instance) {
      if (empty($entity->{$field_name})) {
        unset($output[$field_name]);
      }
    }
    // TODO: don't depend on field name.
    if ($entity_type == 'cv' && !empty($entity->field_cv_photo[$entity->language][0]['is_default'])) {
      unset($output['field_cv_photo']);
    }
    return;
  }
  if (!entity_access('update', $entity_type, $entity)) {
    // Remove empty field. (field collections).
    foreach ($instances as $field_name => $instance) {
      if (empty($entity->{$field_name})) {
        unset($output[$field_name]);
      }
    }
    return;
  }
  $view_mode = $context['view_mode'];

  foreach ($instances as $field_name => $instance) {
    $editable = isset($instance['settings']['editable'][$view_mode]) ?
      $instance['settings']['editable'][$view_mode] : 0;

    // Set content for fields they are empty.
    if (empty($entity->{$field_name}) && $editable) {
      $display = field_get_display($instance, $view_mode, $entity);
      // Do not add field that is hidden in current display.
      if ($display['type'] == 'hidden') {
        continue;
      }
      // Load field settings.
      $field = field_info_field($field_name);

      // Set output for field.
      $output[$field_name] = array(
        '#theme' => 'field',
        '#title' => $t($instance['label']),
        '#label_display' => 'above',
        '#field_type' => $field['type'],
        '#field_name' => $field_name,
        '#bundle' => $entity->type,
        '#object' => $entity,
        '#items' => array(),
        '#entity_type' => $entity_type,
        '#weight' => $display['weight'],
        '#formatter' => '',
        '#suffix' => isset($output[$field_name]['#suffix']) ?
        $output[$field_name]['#suffix'] : '',
        '#prefix' => isset($output[$field_name]['#prefix']) ?
        $output[$field_name]['#prefix'] : '',
        0 => array('#markup' => ''),
      );
    }
  }
}

/**
 * Implements hook_form_field_ui_display_overview_form_alter().
 */
function inline_editor_form_field_ui_display_overview_form_alter(&$form, &$form_state) {
  if (!in_array($form['#entity_type'], variable_get('inline_editor_editable_entities', array()))) {
    return;
  }
  if ($form['#entity_type'] == 'field_collection_item') {
    return;
  }
  $field_instances = field_info_instances($form['#entity_type'], $form['#bundle']);

  foreach ($form['#fields'] as $field_name) {
    $form['fields'][$field_name]['editable'] = array(
      '#type' => 'checkbox',
      '#default_value' => isset($field_instances[$field_name]['settings']['editable'][$form['#view_mode']]) ?
      $field_instances[$field_name]['settings']['editable'][$form['#view_mode']] : 0,
    );
  }

  $form['fields']['#header'][] = array('data' => t('Editable'));
  $form['#submit'][] = 'inline_editor_field_editable_submit';
}

/**
 * Implements submit callback for field settings.
 */
function inline_editor_field_editable_submit($form, $form_state) {
  $field_instances = field_info_instances($form['#entity_type'], $form['#bundle']);
  $form_fields = $form_state['values']['fields'];
  foreach ($field_instances as $field_name => $field_instance) {
    if (!isset($field_instance['settings']['editable']) ||
        !is_array($field_instance['settings']['editable'])) {
      $field_instance['settings']['editable'] = array();
    }

    $field_instance['settings']['editable'][$form['#view_mode']]
      = isset($form_fields[$field_name]['editable']) ?
        $form_fields[$field_name]['editable'] : 0;

    field_update_instance($field_instance);
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * Keeps the editable settings in field instance.
 */
function inline_editor_form_field_ui_field_edit_form_alter(&$form, &$form_state) {
  $form['#submit'][] = 'inline_editor_editable_settings';
}

/**
 * Prevents to override editable settings when a field edit form is submitted.
 */
function inline_editor_editable_settings($form, &$form_state) {
  // Get old instance.
  $oi = &$form['#instance'];
  $editable_settings = isset($oi['settings']['editable']) ?
    $oi['settings']['editable'] : array();

  // We have to load the previous saved version of field instance settings.
  $field_instance = field_info_instance($oi['entity_type'], $oi['field_name'], $oi['bundle']);
  $field_instance['settings']['editable'] = $editable_settings;
  field_update_instance($field_instance);
}

/**
 * Retrieves edit form for specific field.
 */
function inline_editor_get_field_form_ajax($entity_type, $entity_id, $field_name, $view_mode, $lang) {
  $t = get_t();

  $entity = entity_load_single($entity_type, $entity_id);

  // Returns form only if it has the right to edit.
  if (!entity_access('update', $entity_type, $entity)) {
    ajax_deliver(array('#type' => 'ajax', '#commands' => array()));
    return;
  }

  $orig_js = $js = &drupal_static('drupal_add_js');
  $orig_css = $css = &drupal_static('drupal_add_css');

  $form = drupal_get_form('inline_editor_build_field_form', $entity_type, $entity_id, $entity, $lang, $view_mode, $field_name);

  if (isset($form[$field_name][$lang]['#title'])) {
    $form[$field_name][$lang]['#title'] = $t($form[$field_name][$lang]['#title']);
  }

  $ckeditor_utils = '';
  if (module_exists('ckeditor')) {
    // Ckeditor needs utils.js to be loaded.
    $cke_path = drupal_get_path('module', 'ckeditor');
    $ckeditor_utils = $cke_path . '/includes/ckeditor.utils.js';
    $form['#attached']['js'][] = $ckeditor_utils;
  }
  $form_rendered = drupal_render($form);
  $html = _inline_editor_get_css_js($css, $js, $orig_js, $orig_css, $ckeditor_utils);
  $commands = array();
  $commands[] = ajax_command_html('.inline-editor-current-edit', $form_rendered . $html);
  $commands[] = array('command' => 'inlineEditorAfterFormReceived');
  $commands[] = array('command' => 'inlineEditorFocusInput');
  $page = array('#type' => 'ajax', '#commands' => $commands);
  ajax_deliver($page);
}

/**
 * Creates form and attaches the field given in field_name.
 */
function inline_editor_build_field_form($form, &$form_state, $entity_type, $entity_id, $entity, $lang, $view_mode, $field_name = NULL) {
  $form['#entity'] = $entity;
  $form['field_name'] = array(
    '#type' => 'value',
    '#default_value' => $field_name,
  );
  $form['view_mode'] = array(
    '#type' => 'value',
    '#default_value' => $view_mode,
  );
  if ($field_name) {
    $options = array(
      'field_name' => $field_name,
      'language' => $lang,
      'default' => TRUE,
    );
  }

  field_attach_form($entity_type, $entity, $form, $form_state, $lang, $options);
  return $form;
}

/**
 * Validates and saves the form submitted via ajax.
 */
function inline_editor_field_form_ajax_submit(&$form, &$form_state) {
  $t = get_t();

  $commands = array();
  $entity = $form['#entity'];
  $entity_type = $form['#entity_type'];

  if (!entity_access('update', $entity_type, $entity)) {
    $commands[] = array('command' => 'inlineEditorCancelEdit');
    return array('#type' => 'ajax', '#commands' => $commands);
  }

  $field_name = $form_state['values']['field_name'];
  $lang = $form[$field_name]['#language'];
  $field_info = field_info_field($field_name);
  list($entity_id) = entity_extract_ids($entity_type, $entity);

  if ($field_info['type'] == 'image' || $field_info['type'] == 'file') {
    $file = (object) $form_state['values'][$field_name][$lang][0];
    $is_usage = file_usage_list($file);
    if ($file->fid && !$is_usage) {
      file_usage_add($file, 'file', $entity_type, $entity_id);
    }
  }

  // Validate the field.
  $field_array = array('field_name' => $field_name);
  field_attach_form_validate($entity_type, $entity, $form, $form_state, $field_array);

  if (!form_get_errors()) {
    $view_mode = $form_state['values']['view_mode'];

    if ($field_name) {
      field_attach_submit($entity_type, $entity, $form, $form_state, $field_array);
    }
    else {
      field_attach_submit($entity_type, $entity, $form, $form_state);
    }

    entity_save($entity_type, $entity);
    $instance = field_info_instance($entity_type, $field_name, $form['#bundle']);
    $display = field_get_display($instance, $view_mode, $entity);

    // Entity should be loaded again.
    $entity = entity_load_single($entity_type, $entity_id);
    $field = field_view_field($entity_type, $entity, $field_name, $view_mode);

    if (empty($field)) {
      // Load field settings.
      $field_info = field_info_field($field_name);
      $field = array(
        '#theme' => 'field',
        '#title' => $t($instance['label']),
        '#label_display' => 'above',
        '#field_type' => $field_info['type'],
        '#field_name' => $field_name,
        '#bundle' => $entity->type,
        '#object' => $entity,
        '#items' => array(),
        '#entity_type' => $entity_type,
        '#weight' => $display['weight'],
        '#formatter' => '',
        0 => array('#markup' => '&nbsp;'),
      );
    }

    $commands[] = ajax_command_replace('.inline-editor-current-edit', drupal_render($field));
    $commands[] = array('command' => 'inlineEditorAfterFormSubmit');
    return array('#type' => 'ajax', '#commands' => $commands);
  }
  else {
    $form_state['rebuild'] = TRUE;
    // Clear the drupal errors.
    drupal_get_messages('error');
    $commands[] = ajax_command_html('.inline-editor-current-edit', drupal_render($form));
    return array('#type' => 'ajax', '#commands' => $commands);
  }
}

/**
 * Retrieves edit form for field collection item.
 */
function inline_editor_get_field_collection_form_ajax($id, $entity_type, $field_name, $host_type, $host_entity_id, $view_mode) {

  $t = get_t();

  $host_entity = entity_load_single($host_type, $host_entity_id);
  // Returns form only if it has the right to edit.
  if (!entity_access('update', $host_type, $host_entity)) {
    ajax_deliver(array('#type' => 'ajax', '#commands' => array()));
    return;
  }

  if (!$id) {
    $field_array = array('field_name' => str_replace('-', '_', $field_name));
    $field_collection_item = entity_create('field_collection_item', $field_array);
    $field_collection_item->setHostEntity($host_type, $host_entity);
    $target = '#inline-editor-new-item';
  }
  else {
    $field_collection_item = field_collection_item_load($id);
    $target = '.inline-editor-current-edit';
  }

  $orig_js = $js = &drupal_static('drupal_add_js');
  $orig_css = $css = &drupal_static('drupal_add_css');
  module_load_include('inc', 'field_collection', 'field_collection.pages');
  $form = drupal_get_form('field_collection_item_form', $field_collection_item, $view_mode);

  $ckeditor_utils = '';
  if (module_exists('ckeditor')) {
    // Ckeditor needs utils.js to be loaded.
    $cke_path = drupal_get_path('module', 'ckeditor');
    $ckeditor_utils = $cke_path . '/includes/ckeditor.utils.js';
    $form['#attached']['js'][] = $ckeditor_utils;
  }

  $fields = field_info_instances($form['#entity_type'], $form['#bundle']);
  foreach ($fields as $f_name => $field) {
    $lang = $form[$f_name]['#language'];
    if (isset($form[$f_name][$lang]['#title'])) {
      $form[$f_name][$lang]['#title'] = $t($form[$f_name][$lang]['#title']);
    }
  }

  $form_rendered = drupal_render($form);
  $html = _inline_editor_get_css_js($css, $js, $orig_js, $orig_css, $ckeditor_utils);
  $commands = array();
  $commands[] = ajax_command_html($target, $form_rendered . $html);
  $commands[] = array('command' => 'inlineEditorAfterFormReceived');
  $commands[] = array('command' => 'inlineEditorScrollFix');
  $page = array('#type' => 'ajax', '#commands' => $commands);
  ajax_deliver($page);
}

/**
 * Validates and saves the field collection form.
 */
function inline_editor_field_collection_form_submit(&$form, &$form_state, $form_id) {
  module_load_include('inc', 'field_collection', 'field_collection.pages');
  // Validate field collection item.
  field_collection_item_form_validate($form, $form_state);
  $field_collection_item = field_collection_item_form_submit_build_field_collection($form, $form_state);

  // Returns form only if it has the right to edit.
  if (!entity_access('update', 'field_collection_item', $field_collection_item)) {
    $commands[] = array('command' => 'inlineEditorCancelEdit');
    return array('#type' => 'ajax', '#commands' => $commands);
  }

  $is_new = isset($field_collection_item->is_new) ?
    $field_collection_item->is_new : 0;

  $commands = array();

  if (!form_get_errors()) {
    $field_collection_item->save();

    $field_name = $form['field_name']['#value'];
    $view_mode = $form_state['values']['view_mode'];
    if ($is_new) {
      $host_entity = $field_collection_item->hostEntity();
      $host_type = $field_collection_item->hostEntityType();
      $item = field_view_field($host_type, $host_entity, $field_name, $view_mode);
      $target = '.inline-editor-insert-new-item';
    }
    else {
      $item = entity_view('field_collection_item', array($field_collection_item), $view_mode);
      $target = '.inline-editor-edit-in-use';
    }
    $commands[] = ajax_command_replace($target, drupal_render($item));
    $commands[] = array(
      'command' => 'inlineEditorAfterCollectionSubmit',
      'fieldName' => $field_name,
    );
    return array('#type' => 'ajax', '#commands' => $commands);

  }
  else {
    $target = $is_new ? '#inline-editor-new-item' : '.inline-editor-current-edit';
    // Rebuild form to display the errors.
    $form_state['rebuild'] = TRUE;
    // Clear the drupal errors.
    drupal_get_messages('error');
    $commands[] = ajax_command_html($target, drupal_render($form));
    return array('#type' => 'ajax', '#commands' => $commands);
  }
}

/**
 * Implements hook_form_alter().
 */
function inline_editor_form_alter(&$form, &$form_state, $form_id) {
  switch ($form_id) {
    case 'inline_editor_build_field_form':
      $entity = $form['#entity'];
      $callback = 'inline_editor_field_form_ajax_submit';
      break;

    case 'field_collection_item_form':
      $form['view_mode'] = array(
        '#type' => 'value',
        '#default_value' => !empty($form_state['build_info']['args'][1]) ?
        $form_state['build_info']['args'][1] : 'full',
      );
      $entity = $form_state['field_collection_item'];
      $callback = 'inline_editor_field_collection_form_submit';
      break;

    default:
      return;
  }

  $cancel_class = isset($entity->is_new) ?
   'inline-editor-cancel-new-item' : 'inline-editor-cancel-edit';
  $form['actions']['cancel'] = array(
    '#markup' => '<div '
    . drupal_attributes(
      array(
        'class' => array('inline-editor-cancel-btn', $cancel_class),
      )
    ) . '>' . t('Cancel') . '</div>',
    '#weight' => 51,
  );
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#attributes' => array(
      'class' => array(
        'inline-editor-save-edit',
        'use-ajax',
      ),
      'href' => '?q=system/ajax',
    ),
    '#ajax' => array(
      'callback' => $callback,
      'event' => 'click',
    ),
    '#weight' => 50,
  );

  if ($form_id == 'field_collection_item_form' && !isset($entity->is_new)) {
    $form['actions']['delete'] = array(
      '#markup' => '<div'
      . drupal_attributes(
        array(
          'class' => array('field-collection-item-delete'),
          'item-id' => $entity->item_id,
        )
      ) . '>' . t('Delete') . '</div>',
      '#weight' => 52,
    );
  }

  unset($form['#validate'], $form['#submit']);
}

/**
 * Delete field collection item.
 */
function inline_editor_delete_field_collection_item() {
  $id = check_plain($_POST['data']);
  $field_collection_item = field_collection_item_load($id);
  $entity = $field_collection_item->hostEntity();
  if ($entity) {
    if (entity_access('delete', $entity->type, $entity)) {
      drupal_json_output(array('deleted' => 0));
      return;
    }
    $field_collection_item->delete();
    drupal_json_output(array('deleted' => 1));
    return;
  }

  drupal_json_output(array('deleted' => 0));
}

/**
 * Implements hook_field_attach_submit().
 *
 * Remove deleted files.
 */
function inline_editor_field_attach_submit($entity_type, $entity, &$form, &$form_state) {
  $info = $form_state['build_info'];
  if ($info['form_id'] == 'inline_editor_build_field_form' &&
      !empty($form_state['has_file_element'])) {
    $old_entity = $info['args'][2];
    $field_name = $info['args'][5];
    list($id) = entity_extract_ids($entity_type, $entity);
    $lang = $old_entity->language;
    $old_fid = $old_entity->{$field_name}[$lang][0]['fid'];

    if ($old_fid && $old_fid != $form_state['values'][$field_name][$lang][0]['fid']) {
      if ($file = file_load($old_fid)) {
        file_usage_delete($file, 'file', $entity_type, $id);
        file_delete($file);
      }
    }
  }
}

/**
 * Helper function for getting css and js scripts.
 */
function _inline_editor_get_css_js($css, $js, $orig_js, $orig_css, $ckeditor_utils) {
  $diff_js = array_diff_assoc($js, $orig_js);
  if (module_exists('ckeditor') && isset($diff_js[$ckeditor_utils])) {
    unset($_SESSION['js_css_already_loaded'][$ckeditor_utils]);
  }

  if (isset($_SESSION['js_css_already_loaded'])) {
    $diff_js = array_diff_assoc($diff_js, $_SESSION['js_css_already_loaded']);
    $_SESSION['js_css_already_loaded'] = array_merge($diff_js, $_SESSION['js_css_already_loaded']);
  }
  else {
    $_SESSION['js_css_already_loaded'] = $diff_js;
  }

  $html  = drupal_get_js('header', $diff_js) . drupal_get_js('footer', $diff_js);
  $html .= drupal_get_css(array_diff_assoc($css, $orig_css));

  return $html;
}
